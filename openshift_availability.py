#!/usr/bin/env python

import click
import logging
import os
import requests
import time
import json
import sys
from cernservicexml import Status

from config import PROMETHEUS_QUERIES_NUMERIC_VALUES
from config import OPENSHIFT_SLS_DEGRADED_QUERY
from config import OPENSHIFT_ENDPOINT
from prometheus_http_api import PrometheusAPI

SLS_ENDPOINT = 'http://monit-metrics:10012/'

def obtain_openshift_availability(prometheus_api):
    """
    Criteria:
    - Degraded
        - The ratio of free memory in standard, ready and schedulable nodes is less than a given threshold
    - Unavailable
        - master API unavailable via HAProxy (openshift.cern.ch)

    Returns a tuple containing the cernservicexml.Status and a dict of numeric_values
    in the format of {query: query_result}
    """

    status = Status.available
    numeric_values = {name: float(prometheus_api.single_value_query(query))
                      for name, query in PROMETHEUS_QUERIES_NUMERIC_VALUES.items()}

    # DEGRADED QUERY
    # Check ratio of free memory in standard, ready and schedulable nodes
    value = prometheus_api.single_value_query(OPENSHIFT_SLS_DEGRADED_QUERY)
    if value and int(value) >= 1:
        logging.info(
            'There are capacity alarms firing, service is DEGRADED ...')
        status = Status.degraded
    else:
        logging.info(
            'There are no capacity alarms firing, service is AVAILABLE ...')

    # UNAVAILABLE QUERY
    # Check openshift-api through the router
    # Obtain token to query
    path = "/var/run/secrets/kubernetes.io/serviceaccount"
    with open(os.path.join(path, "token")) as fp:
        token = fp.read()
    try:
        resp = requests.get(OPENSHIFT_ENDPOINT + '/oapi/v1/users/~', headers={
                            "Authorization": "Bearer " + token}, verify=False, timeout=120)
        if resp.status_code != 200:
            logging.info(
                'Openshift API returned %s, marking service as UNAVAILABLE' % resp.status_code)
            status = Status.unavailable
        else:
            logging.info('Openshift API returned %s, successful check' %
                         resp.status_code)
    except requests.exceptions.RequestException as ex:  # This is the correct syntax
        logging.info('Cannot contact the OpenShift API: %s ' % repr(ex))
        status = Status.unavailable

    return status, numeric_values


def generate_service_monite_json(prometheus_api, dry_run):
    """
    Generate XML file and send it to SLS (unless dry_run is enabled)
    """

    status, numeric_values = obtain_openshift_availability(prometheus_api)

    timestamp = int(round(time.time() * 1000))
    document = [{
        "producer": "openshift",
        "type": "availability",
        "serviceid": "paas",
        "service_status": status,
        "contact": "https://cern.service-now.com/service-portal/?id=service_element&name=PaaS-Web-App",
        "webpage": OPENSHIFT_ENDPOINT,
        "timestamp": timestamp
    }]

    document_kpi = [{
        "producer": "openshift",
        "type": "kpi",
        "number_of_schedulable_nodes": numeric_values['number_of_schedulable_nodes'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_schedulable_nodes"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_of_running_pods": numeric_values['number_of_running_pods'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_running_pods"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "total_memory_used_mb": numeric_values['total_memory_used_mb'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["total_memory_used_mb"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "router_bandwidth": numeric_values['router_bandwidth'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["router_bandwidth"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_of_user_projects": numeric_values['number_of_user_projects'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_of_user_projects"]
        },{
        "producer": "openshift",
        "type": "kpi",
        "number_cephfs_pvs": numeric_values['number_cephfs_pvs'],
        "openshift-project": "openshift",
        "environment":"production",
        "idb_tags": ["openshift-project","environment"],
        "idb_fields": ["number_cephfs_pvs"]
        }
    ]

    if not dry_run:
        response = requests.post(SLS_ENDPOINT, data=json.dumps(document),
                                 headers={"Content-Type": "application/json; charset=UTF-8"})

        response_kpi = requests.post(SLS_ENDPOINT, data=json.dumps(document_kpi),
                                     headers={"Content-Type": "application/json; charset=UTF-8"})

    if response.status_code != 200 or response_kpi.status_code != 200:

        logging.error('Error executing POST statement. Status code: {0}. Message: {1}'.format(
            response.status_code, response.text))
        sys.exit(1)
    else:
        logging.info('Metric sent: %s' % document)
        logging.info('Metric KPI sent: %s' % document_kpi)

@click.command()
@click.option('--daemon', default=10, help='Run the application in daemon mode, waiting the minutes passed as argument. If 0 is passed it will not run in daemon mode')
@click.option('--dry-run', is_flag=True, help='Obtain monit json but do not send it to monit')
def run_openshift_availability(daemon, dry_run):

    prometheus_api = PrometheusAPI()

    while daemon:
        logging.info('Running in daemon mode')
        if dry_run:
            logging.info("Running in dry-run mode, monit info will NOT be sent to central monit...")
        generate_service_monite_json(prometheus_api, dry_run)
        logging.info('Waiting during %s minutes ' % daemon)
        time.sleep(daemon * 60)
    else:
        logging.info('Run in normal mode')
        generate_service_monite_json(prometheus_api, dry_run)


if __name__ == "__main__":
    run_openshift_availability()
