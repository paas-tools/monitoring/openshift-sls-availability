OpenShift availability
=================

OpenShift availability is calculated by obtaining relevant data from Prometheus,
generates a json with all the information relevant and sends it to monit-metrics.cern.ch.

# Status of the service

The script measures the availability of the service and sets it to three different values:

* *Unavailable*: If the OpenShift API cannot be contacted through a Route (veryfing HAProxy
keeps working)
* *Degraded*: If the memory available in Standard, Schedulable and Ready nodes is below
a given threshold. This means new apps would struggle to find room to run.
* *Available*: If all the checks aboves pass correctly.

## Additional metrics

In addition to the availability, there are a few metrics been sent to SLS.

* Number of schedulable pods
* Number of running pods
* Total CPU cores requested by applications
* Total memory in Mb requested by applications
* Actual memory usage of all pods

# Service XML file example

```xml
<serviceupdate xmlns="http://sls.cern.ch/SLS/XML/update">
  <timestamp>2015-07-14T12:07:47</timestamp>
  <data>
    <numericvalue name="number_of_schedulable_nodes">14</numericvalue>
    <numericvalue name="number_of_running_pods">501</numericvalue>
    <numericvalue name="total_cpu_requested">...</numericvalue>
    <numericvalue name="total_memory_requested">...</numericvalue>
    <numericvalue name="actual_memory_usage">...</numericvalue>
  </data>
  <id>PaaS</id>
  <status>available</status>
  <availabilityinfo>available</availabilityinfo>
</serviceupdate>
```

# Deployment

These application is deployed in the `paas-monitoring` namespace.

```
oc create -f openshift_availability.yaml -n paas-monitoring
```

Please note that this application is only deployed in the production cluster (ie https://openshift.cern.ch)
