import requests
import logging

from config import PROMETHEUS_DEFAULT_HOST, PROMETHEUS_DEFAULT_PORT

class PrometheusAPI(object):
    """Python binding for the HTTP Prometheus API"""
    def __init__(self, host=None, port=None):
        host = host or PROMETHEUS_DEFAULT_HOST
        port = port or PROMETHEUS_DEFAULT_PORT
        if host and port:
            self.endpoint = 'http://%s:%s' % (host, port)
        else:
            self.endpoint = 'http://localhost'

    def query(self, query):
        return requests.get(self.endpoint + '/api/v1/query', timeout=60, params={'query': query})

    def single_value_query(self, query):
        """
        Extract the single value from the JSON structure of the Prometheus API
        """
        resp = self.query(query).json()
        # Extract the value from the Prometheus Json structure
        # The response looks like:
        # {'status': 'success', 'data': {'resultType': 'vector', 'result': [{'value': [1501595028.608, '205'], 'metric': {}}]}}
        try:
            return resp['data']['result'][0]['value'][-1]
        except IndexError:
            logging.error('Response query does not have a value in the expected field. Returning None')
            return None
