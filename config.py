import logging
import os

# logging setup
LOG_FORMAT = '[%(asctime)-15s] %(levelname)-8s %(funcName)s: %(message)s'
logging.basicConfig(format=LOG_FORMAT, level=logging.DEBUG)

# PROMETHEUS endpoint
PROMETHEUS_DEFAULT_HOST = os.environ.get('PROMETHEUS_SERVICE_HOST', '')
PROMETHEUS_DEFAULT_PORT = os.environ.get('PROMETHEUS_SERVICE_PORT', '')

OPENSHIFT_ENDPOINT = os.environ.get('OPENSHIFT_ENDPOINT', 'https://openshift.cern.ch')

# Prometheus queries
PROMETHEUS_QUERIES_NUMERIC_VALUES = {
    'number_of_schedulable_nodes': 'count(min(kube_node_spec_unschedulable==0)by(node))',
    'number_of_running_pods': 'count(kube_pod_status_phase{phase=~"Running|Terminating"})',
    'total_memory_used_mb': 'sum(container_memory_usage_bytes{name=~"k8s_.+"})/1000000',
    'router_bandwidth': '(sum(rate(haproxy_backend_bytes_in_total[10m])) + sum(rate(haproxy_backend_bytes_out_total[10m])))*8/1000000',
    'number_of_user_projects': 'count(kube_namespace_status_phase{phase="Active",namespace!~"kube-.+|openshift.*|default|logging|management-infra|paas-infra.*|gitlab|web-infrastructure|webeos"})',
    'number_cephfs_pvs': 'count(kube_persistentvolume_info{storageclass=~"cephfs.*"})'
}

# If any of the capacity alarms in prometheus is firing, mark service as degraded.
# This usually means the current schedulable, standard and ready nodes are not sufficient
# to run the service.
OPENSHIFT_SLS_DEGRADED_QUERY = 'count(ALERTS{alertname="openshift_standard_nodes_available_memory",alertstate="firing"} or ALERTS{alertname="openshift_standard_nodes_allocatable_memory",alertstate="firing"} or ALERTS{alertname="openshift_standard_nodes_allocatable_cpu",alertstate="firing"})'
